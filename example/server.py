from typing import Optional

from fastapi import FastAPI, Header

app = FastAPI()


@app.get("/api/hello")
async def root(x_goog_iap_jwt_assertion: str = Header(None)):
    return {"token-jwt": x_goog_iap_jwt_assertion}