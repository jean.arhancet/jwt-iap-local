
# IAP JWT LOCAL

 
This proxy add a JWT IAP token for you ❤️

When you use IAP, Google will handle the application-level access control model for you by default by making sure only those users or groups are allowed through.  

IAP : 
> user --> IAP --> your app

Locally with Envoy:
> Front --ENVOY--> Back end

## Configuration

In `envoy_iap.yaml`, it's important to change **line 47-49**, PROJECT, EMAIL and GOOGLE_ID 

	   -- Configuration for JWT token (GCP PROJECT_ID, EMAIL, GOOGLE_ID)
	   local project_id = "<PROJECT_ID>"
	   local email = "<EMAIL>"
	   local google_id = "<GOOGLE_ID>"


## Installation

1. Install [Envoy](https://www.envoyproxy.io/docs/envoy/latest/start/install)
2. Change configuration
3. Start envoy with `envoy_iap.yaml` :
```bash
envoy -c envoy_iap.yaml
```
4. Enjoy 🎉